## Getting started

* This Repository consists an example of community modelling notebook with workflows to run variety of models in the jupiterhub environment on EcoCommon's coding cloud. 
* This example will allow users to load and play with different example workflows. Furthermore, users can add more workflows and examples which can be shared with colleagues and wider community.

---
Author details:  EcoCommons Platform   
Contact details: comms@ecocommons.org.au  
Copyright statement: This script is the product of the EcoCommons platform.   
                     Please refer to the EcoCommons website for more details:   
                     <https://www.ecocommons.org.au/>  
Date: September 2023  

---

## Script and Data information 

This notebook provides an example of how to run a Community Modelling module using the R-package `cmGDM`, created by Peter D. Wilson.

Generalised Dissimilarity Modelling (GDM) has become a widely-used method to link differences in composition
between samples and explanatory environmental variables (“covariates”). The dependent or predicted variable
in a GDM may be any form of distance or dissimilarity measure scaled to range between 0 (exactly matching pairs)and 1 (totally dissimilar pairs). GDMs may be fitted using any assemblage of covariates thought to be important in explaining the differences in composition between pairs of samples. The prototype R-package, `cmGDM` is designed to implement GDM
modelling as the first method within a new EcoCommons Community Modelling module. Fitting a GDM is performed by the R-package `gdm` available from the [CRAN repository](https://cran.r-project.org/web/packages/gdm/index.html) . `cmGDM` has been designed to implement a simple, robust workflow for basic fitting, review and reporting of GDMs.

In the near future, this material may form part of comprehensive support materials available to EcoCommons users. If you have any corrections or suggestions, please [contact the EcoCommons](mailto:comms@ecocommons.org.au) support and communications team.


## GDM Fundamentals
The fundamental data elements required to fit a GDM are:
- site/sample ID and location data
- a suite of environmental predictors or covariates associated with each site/sample
- data on the composition of entities at each site/sample, or a pre-computed dissimilarity matrix giving a dissimilarity measure between each pair of sites or samples

The package `cmGDM` accepts three basic table formats, where users can generate pre-processed
or derived tables by asking users to supply necessary information. For example, users will be asked to load
community data as either:

- presence-absence table
- abundance table
- dissimilarity table

In the first two table types, they are standard tables which may be assembled by hand (in spreadsheets for example) or
output by functions in other R-packages. For dissimilarity tables, many functions in other R-packages can
produce dissimilarity tables including the R-package `vegan`, and several packages for analysing phylogenetic
and population genetic data. The path to data importation chosen during the development of `cmGDM` is to follow procedures referred to as “bioformat = 3” in the `gdm` package.

The workflow embodied in the `cmGDM` package is a simple, but strict, linear sequence designed to ensure maximum
data integrity whilst providing a reasonable level of flexibility regarding data format options for each data element.


## Workflows

Users will find four examples to run 

- A. Species composition example: Presence-absence data 
- B. Species composition example: Presence-absence data and environmental data as GIS layers  
- C. Species composition example: Abundance data, richness site weights and environmental data as GIS layers  
- D. Genetic diversity example


## Add your files

This is a public repository. Please follow the below steps in order to add more notebooks and examples for community modelling.

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ecocommons-australia/ecocommons-platform/example-notebooks.git
git branch -M main
git push -uf origin main
```

## Support

Please contact the EcoCommons team for testing or any support with this notebook.
